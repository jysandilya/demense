(defproject polls "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/java.jdbc "0.7.5"]
                 [bidi "2.1.3"]
                 [org.postgresql/postgresql "42.1.4"]
                 [korma "0.4.3"]
                 [mount "0.1.11"]
                 [medley "1.0.0"]
                 [aero "1.1.2"]
                 [cheshire "5.8.0"]
                 [camel-snake-kebab "0.4.0"]
                 [ring "1.6.3"]
                 [ring/ring-core "1.6.3"]
                 [ring/ring-defaults "0.3.1"]
                 [ring/ring-json "0.4.0"]]
  :main ^:skip-aot polls.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
