(ns demense.models.fields)

(def possible-types #{::int ::string ::serial})

(defn attribute
  [name type & constraints]
  {:name        name
   :type        type
   :constraints (conj (set constraints) "NOT NULL")})

(defn primary-key
  [name type & constraints]
  (apply attribute name type "PRIMARY KEY" constraints))

(defn primary-key?
  [{:keys [constraints] :as field}]
  (contains? (set constraints) "PRIMARY KEY"))
