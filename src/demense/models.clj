(ns demense.models
  (:require [demense.models.fields :as fields]))

(defn primary-key
  [{:keys [fields] :as model}]
  (->> fields
       (filter fields/primary-key?)
       first))
