(ns demense.util.coerce
  (:require [schema.core :as s]
            [camel-snake-kebab.core :as csk]
            [medley.core :as medley]
            [clojure.walk :as w]
            [demense.models.fields :as fields])
  (:import (schema.core OptionalKey)))

(defn ->double
  [s]
  (try
    (when s
      (Double/parseDouble s))
    (catch java.lang.NumberFormatException _
      nil)))

(defn ->int
  [s]
  (try
    (when s
      (Integer/parseInt s))
    (catch java.lang.NumberFormatException _
      nil)))

(defn ->bigint
  [s]
  (try
    (when s
      (bigint s))
    (catch java.lang.NumberFormatException _
      nil)))

(defn- update-some
  "Updates a key in a map only if that key is present."
  [m k f]
  (if (contains? m k)
    (update m k f)
    m))

(defn- update-keys
  "Updates only the given keys in a map."
  [params keyseq update-fn]
  (reduce (fn [acc k] (update-some acc k update-fn))
          params
          keyseq))


(defn attrs->double
  [m keyseq]
  (update-keys m
               keyseq
               ->double))

(defn attrs->int
  [m keyseq]
  (update-keys m
               keyseq
               ->int))

(defn kebab->snake-string
  [args]
  (csk/->snake_case_string args :separator \-))

(defn ->keyword
  [arg]
  (cond
    (instance? OptionalKey arg) (s/optional-key (->keyword (:k arg)))
    :else (csk/->kebab-case-keyword arg)))

(defn keywordize
  [nested-map]
  (w/postwalk #(if (map? %)
                 (medley/map-keys ->keyword %)
                 %)
              nested-map))

(defn underscorize
  [nested-map]
  (w/postwalk #(if (map? %)
                 (medley/map-keys kebab->snake-string %)
                 %)
              nested-map))

(defn coerce-to-field-type
  [v field-type]
  (case field-type
    ::fields/string v
    ::fields/int (->int v)
    ::fields/serial (->bigint v)
    :default nil))
