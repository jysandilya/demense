(ns demense.routes
  (:require [bidi.ring :as bidi]
            [demense.handlers :as handlers]))

(defn- route-prefix
  [{:keys [name] :as model}]
  (str name "/"))

(defn list-route
  [model]
  [(route-prefix model) {:get (handlers/list-handler model)}])

(defn create-route
  [model]
  [(route-prefix model) {:post (handlers/create-handler model)}])

(defn detail-route
  [model]
  [[(route-prefix model) :id "/"] {:get (handlers/detail-handler model)}])

(defn update-route
  [model]
  [[(route-prefix model) :id "/"] {:put (handlers/update-handler model)}])

(defn delete-route
  [model]
  [[(route-prefix model) :id "/"] {:delete (handlers/delete-handler model)}])

(defn crud-routes
  [model]
  [(list-route model)
   (create-route model)
   (detail-route model)
   (update-route model)
   (delete-route model)])
