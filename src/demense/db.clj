(ns demense.db
  (:require [clojure.java.jdbc :as jdbc]
            [camel-snake-kebab.core :as csk]
            [demense.models :as models]
            [demense.models.fields :as fields]
            [demense.util.coerce :as uc]))


(def ^:private db-spec {:connection-uri "jdbc:postgresql://localhost:5433/polls"})

(defn get-spec [] db-spec)

(def sql-type {::fields/int    "integer"
               ::fields/serial "bigserial"
               ::fields/string "text"})

(defn- column-spec
  [{:keys [name type constraints] :as field}]
  (into [(csk/->snake_case_string name) (sql-type type)] constraints))

(defn create-ddl
  [{:keys [name fields]}]
  (jdbc/create-table-ddl name (map column-spec fields)))

(defn create-table!
  [model]
  (jdbc/execute! (get-spec) (create-ddl model)))

(defn fetch-by
  [model columns]
  (jdbc/find-by-keys (get-spec)
                     (:name model)
                     columns))

(defn fetch-by-pk
  [model pk]
  (first
    (fetch-by model
              {(-> model
                   models/primary-key
                   :name) pk})))

(defn fetch-all
  [model]
  (jdbc/query (get-spec)
              [(str "SELECT * FROM " (:name model))]))

(defn create!
  [model row]
  (first (jdbc/insert! (get-spec)
                       (:name model)
                       (uc/underscorize row))))

(defn- pk-name
  [model]
  (-> model
      models/primary-key
      :name))

(defn- pk-where-clause
  [model pk]
  [(str (csk/->snake_case_string (pk-name model))
        " = ?")
   pk])

(defn update!
  [model row]
  (let [pk (get row (pk-name model))]
    (when (< 0 (first
                 (jdbc/update! (get-spec)
                               (:name model)
                               (uc/underscorize row)
                               (pk-where-clause model pk))))
      (fetch-by-pk model (or (get row (pk-name model))
                             pk)))))

(defn persist!
  [model row]
  (let [pk (get row (pk-name model))]
    (if (fetch-by-pk model pk)
      (update! model row)
      (create! model row))))

(defn delete!
  [model pk]
  (jdbc/delete! (get-spec) (:name model) (pk-where-clause model pk)))
