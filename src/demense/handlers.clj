(ns demense.handlers
  (:require [ring.util.response :as rur]
            [demense.db :as db]
            [demense.models :as models]
            [demense.util.coerce :as uc]
            [ring.middleware.defaults :as ring-defaults]
            [ring.middleware.json :as ring-json]))

(defn- wrap-api-middleware
  [handler]
  (-> handler
      ring-json/wrap-json-params
      ring-json/wrap-json-response
      (ring-defaults/wrap-defaults ring-defaults/api-defaults)))

(defn list-handler
  [model]
  (-> (fn [request]
        (rur/response (db/fetch-all model)))
      wrap-api-middleware))

(defn create-handler
  [model]
  (-> (fn [{:keys [params]}]
        (-> (db/persist! model params)
            rur/response
            (rur/status 201)))
      wrap-api-middleware))

(defn detail-handler
  [model]
  (fn [{:keys [params] :as request}]
    (let [pk-type (:type (models/primary-key model))]
      (if-let [id (-> params
                      :id
                      (uc/coerce-to-field-type pk-type))]
        (if-let [data (db/fetch-by-pk model id)]
          data
          {:status 404})
        {:status 400}))))

(defn update-handler
  [model]
  (fn [{:keys [params] :as request}]
    (let [{:keys [type name]} (:type (models/primary-key model))]
      (if-let [id (-> params
                      :id
                      (uc/coerce-to-field-type type))]
        (if-let [data (db/persist! model
                                   (assoc params name id))]
          data
          {:status 404})
        {:status 400}))))

(defn delete-handler
  [model]
  (fn [{:keys [params] :as request}]
    (let [pk-type (:type (models/primary-key model))]
      (if-let [id (-> params
                      :id
                      (uc/coerce-to-field-type pk-type))]
        (if-let [data (db/delete! model id)]
          data
          {:status 404})
        {:status 400}))))
