(ns polls.core
  (:require [ring.adapter.jetty :as ring-jetty]
            [polls.routes :as routes]
            [ring.util.response :as rur])
  (:gen-class))

(defonce server (atom nil))

(defn start-server []
  (reset! server (ring-jetty/run-jetty (routes/make-handler)
                                       {:port  8080
                                        :join? false})))

(defn stop-server []
  (.stop @server))

(defn restart-server []
  (stop-server)
  (start-server))

(defn -main
  [& args]
  (start-server))
