(ns polls.models
  (:require [demense.models.fields :as fields]))

(def polls {:name   "polls"
            :fields [(fields/primary-key :id ::fields/serial)
                     (fields/attribute :name ::fields/string)]})
