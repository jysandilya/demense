(ns polls.routes
  (:require [bidi.ring :as bidi]
            [demense.routes :refer [crud-routes]]
            [polls.models :as models]))

(def routes
  ["/" (conj (crud-routes models/polls)
             [true (constantly {:status 404})])])

(defn make-handler []
  (-> routes
      bidi/make-handler))
