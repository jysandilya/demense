(ns demense.db-test
  (:require [clojure.test :refer :all]
            [demense.db :as db]
            [demense.models.fields :as fields]
            [clojure.java.jdbc :as jdbc]))

(defn- test-db-fixture
  [f]
  (jdbc/with-db-transaction [tx {:connection-uri
                                 "jdbc:postgresql://localhost:5433/demense_test"}]
    (jdbc/db-set-rollback-only! tx)
    (with-redefs [db/db-spec tx]
      (f))))

(use-fixtures :each test-db-fixture)

(deftest create-persist-retrieve
  (testing "Can create a table, persist data and retrieve it"
    (let [test-model {:name   "test"
                      :fields [(fields/primary-key :id ::fields/serial)
                               (fields/attribute :baz ::fields/int)]}]
      (db/create-table! test-model)
      (let [persisted-data (db/persist! test-model {:baz 69})]
        (is (= 69
               (:baz persisted-data)))
        (is (= persisted-data (db/fetch-by-pk test-model (:id persisted-data))))))))

(deftest update-with-persist
  (testing "Can update existing data using persist!"
    (let [test-model {:name   "test"
                      :fields [(fields/primary-key :id ::fields/serial)
                               (fields/attribute :baz ::fields/int)]}]
      (db/create-table! test-model)
      (let [persisted-data (db/persist! test-model {:baz 69})]
        (db/persist! test-model (assoc persisted-data :baz 420))
        (is (= 420 (:baz (db/fetch-by-pk test-model
                                         (:id persisted-data)))))))))

(deftest delete
  (testing "Can delete data that was persisted using delete!"
    (let [test-model {:name   "test"
                      :fields [(fields/primary-key :id ::fields/serial)
                               (fields/attribute :baz ::fields/int)]}]
      (db/create-table! test-model)
      (let [persisted-data (db/persist! test-model {:baz 69})]
        (db/delete! test-model (:id persisted-data))
        (is (nil? (db/fetch-by-pk test-model
                                  (:id persisted-data))))))))

(deftest fetch-by-multiple-keys
  (testing "Can filter on multiple keys using fetch-by"
    (let [test-model {:name   "test"
                      :fields [(fields/primary-key :id ::fields/serial)
                               (fields/attribute :baz ::fields/int)
                               (fields/attribute :quux ::fields/string)]}]
      (db/create-table! test-model)
      (db/persist! test-model {:baz  69
                               :quux "foo"})
      (db/persist! test-model {:baz  69
                               :quux "goo"})
      (is (= [{:baz  69
               :quux "goo"}]
             (->> (db/fetch-by test-model {:baz  69
                                           :quux "goo"})
                  (map #(dissoc % :id)))))
      (is (= #{{:baz  69
                :quux "goo"}
               {:baz  69
                :quux "foo"}}
             (->> (db/fetch-by test-model {:baz 69})
                  (map #(dissoc % :id))
                  (set)))))))
